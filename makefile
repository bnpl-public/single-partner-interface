ifdef OS
  COPY := copy
else
  COPY := cp
endif

env: .env
	docker-compose up --build --force-recreate -d && docker-compose logs -f

test: .env
	docker-compose build
	docker-compose run --rm test

seed-partner: .env
	docker-compose run --rm test yarn data:seed-partner

.env:
	$(COPY) .ephemeral.env .env

# Zip Single Partner Interface
## Local Docker environment

## Introduction
This repository is publicly accessible and will act as a shared resource until Twisto colleagues gain full Zip access to Gitlab.

Its purpose is to spin up a copy of our Single Partner Interface proxy in a local Docker environment. Either a real API or mock API can then be added to the environment, and integration tests can be run against it.

## Requirements
 - Docker
 - Access to the Zip BNPLaaS team's shared Azure Container Registry (see @duncan.wraight on Slack)

## Real or Mock API, Docker image or standalone environment
In our work with the containerised SPI instance, and for the purpose of our integration tests, we have used a mock API service. This service responds to the SPI's requests with pre-populated data.

To use a similar approach, with a dockerised API layer, populate the `API_IMAGE...` variables in the `.env` file. You will then need to change your `docker-compose up` command; see the `Start-up` section.

If you want to use a standalone API layer, simply ensure that it is accessible to your local device on a specified port then change the `MOCK_API_URL` variable in `.env`. For example if your Django application was running on port 8000, you would change this to `http://host.docker.internal:8000/api/base/route`. The `host.docker.internal` hostname resolves to the IP of your local device from within the SPI container.

## Start-up

### IMPORTANT NOTE: Linux vs. Mac/Windows
Please check the comments in `docker-compose.yml` - the SPI container's command has to change if you're using Linux. The default command works for Mac and Windows (under WSL).

#### Docker API layer
 1. Change the `.env` file to include your API image (see previous section) by altering the `API_IMAGE...` variables and uncommenting the relevant `MOCK_API_URL`
 2. To ensure you have the latest test and SPI images, run `docker-compose pull`
 3. Run `docker-compose -f docker-compose.yml -f docker-compose-api.yml up -d && docker-compose logs -f`

#### Standalone API layer
 1. Change the `.env` file's `MOCK_API_URL` variable to point at your API layer (see previous section)
 2. To ensure you have the latest test and SPI images, run `docker-compose pull`
 3. Run `make env` (this is basically a shortcut for `docker-compose up -d && docker-compose logs -f`)

The Docker Compose environment will then do the following:
 - Create a local DynamoDB instance
 - Use the AWS CLI image to create some tables within this instance
 - Create a local SPI instance

## Integration tests
As of 2nd Dec 2021, we have now made our SPI integration tests publicly accessible.

These tests will run as part of the `docker-compose up` command, and their output can be viewed by running `docker-compose logs test`. To see more detail, or re-run the tests, use `docker-compose run test`.

## Seeding the DynamoDB tables and generating an authentication token
 1. Run the command `make env` to bring the Compose environment up with latest images
 2. Run `make seed-partner` to populate the DynamoDB and generate an authentication token

This should output a JSON object such as:

```json
{"partnerId":"c5285b3f-d491-4227-a9fd-3f50162ae07f","merchantId":"d6e94f67-a172-4316-bb96-37724e72dbce","spiToken":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJjNTI4NWIzZi1kNDkxLTQyMjctYTlmZC0zZjUwMTYyYWUwN2YiLCJqdGkiOiJjNTI4NWIzZi1kNDkxLTQyMjctYTlmZC0zZjUwMTYyYWUwN2YiLCJpYXQiOjE2Mzg1MjY2MzF9.BLjJ_PdOmm9LGvBRsIa43OZ9Mv953Xark0CF7ciDuzkPARH8sP9rNxVFAqvsotHO5OmzVdDwQjGhvUqqC9K1dqHvsaOEO1R8imv5HAo5gHM0x2khqxTwp_FawUaFPDrAaTkU_2_G1-ygnVmNnKT8l3jxcborP620ADrAcwCyGnKboTIHBnk_eHXmUGhWpddEw8q7JmwOyBhKHPgi8WtdrGTrkQvB1R0RDb-keLIaO4MQjmDIVBmcfkkCjH77MBxWnfBKTvKQ8y2aUXKG4b0I6E9j-5wCH4YDkma4NFi-aQLr59iWZaKV5NVJw9gJAckfS6RUzEtO-Qj6QWS0-YPdb-IbW6sSmwazPDewjdqanaD-XAj4EnJztC5wo_siIVzf4a4TRleTr42aRjygsar38-_Zb6FI66SLWp95b3nTiwk9DJc3Kc0J7NBCHA0vhE-_ARzJX94UPMisrS1Mp9bMWb46GVRLC6yqzrLf0KJIkpK6a2okjzoX38FkefD52eds_pIBIl5YG0Z6MnO4DQoo7l3_B3R1QPRG_gChgtOw_b0-MUIooPAy1nEdrrYQLO8XoliKxzCXM34etu3wc0F5vUP8ql2N9862c0iYJcvMPg3LjC0AOiknbz3aZKSS17zvTLMs8x8OOSmVw8VaIDz8-oO0ltz9KxK__HjEdj2OEZg"}
```

## Sending a request to the SPI -> API

Once you have run the `make seed-partner` command as listed above, you will now be able to send requests to the SPI and hopefully have them reach your API layer.

 1. Copy the `spiToken` from the `make seed-partner` output
 2. From your local machine's terminal, run `curl -XPOST -H "Content-Type: application/json" -H 'Authorization: Bearer SPI-TOKEN-GOES-HERE' --data '{"iso_country_code": "gb"}' http://127.0.0.1:3000/apply`

The expected response from this is a JSON object like...
```
{"apply_link":"https://api-ci.zipco.xyz/partner-mock?sessionToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXNzaW9uSWQiOiI1ODY4OTJhOC0xMTIwLTQzZjgtODJiZi1mN2JmODFmODg4MWYiLCJuYmYiOjE2Mzg5NjE3NjYsImV4cCI6MTY0MTU1Mzc2NiwiaWF0IjoxNjM4OTYxNzY2fQ.2hsqJRJNjp8JItRZPESioFiDqDu2xvzcmgLLZh6o_-8"}`
```

If you receive a 500 response at this point, we need to test communication between the SPI and your API layer, especially if you're running the API layer separately.

 - Run `docker-compose exec spi ping -c2 host.docker.internal` to ensure communication from container to host machine is working as expected
 - Change the URL in the command below to match your local API. `host.docker.internal` should resolve to your machine's IP address from within the container, so just change the port and API path
 - Run `docker-compose exec spi wget -qO- 'http://host.docker.internal:8080/YOUR/API/ROOT/PATH/linkaccount/url' --header 'Authorization: Bearer SPI-TOKEN-GOES-HERE' --header 'Content-Type: application/json' --post-data '{"iso_country_code": "zp"}'`
 - You should be able to see this request in your API logs 
